#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>

#include "database.h"

#define PORT 7379
#define QUEUE_SIZE 5
#define BUFFER_SIZE 256
#define N_SUPPORTED_INSTRUCTIONS 4
#define END_MESSAGE "QUIT"

typedef struct thread_args{
  int socket;
  struct sockaddr_in addr;
}thread_args;

typedef struct faux_entry{
  char key[BUFFER_SIZE];
  char value[BUFFER_SIZE];
  int lifetime;
}faux_entry;

sem_t database_access_semaphore; //to grant access to database only one client at atime

Database database;
int server_socket;

int message_size = 0;
int segment_size = 0;

const char* instructions[] = {"CLIENT", "GET", "SET", "EX"};
int i_flags[] = {0, 0, 0, 0, 0};
char return_msg[BUFFER_SIZE];

void exit_with_error(const char * msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

void int_handler(int sig){
  printf("closing\n");
  database_print(&database);
  close(server_socket);
  database_destroy(&database);
  sem_destroy(&database_access_semaphore);
  
  _exit(0);

}

void receive_message_client(int socket, char* buffer){
  int ret;
  char* aux = buffer;
  
  do{//receive a character at a time for lenght of message is not known in advance
    ret = recv(socket, (void*)aux, sizeof(char), 0);
    
    if(ret == -1){
      exit_with_error("error: recv()\n");
    }
    
    if(ret == 0){
      printf("conenction closed\n");
      memcpy(buffer, END_MESSAGE, strlen(END_MESSAGE));
      return;
    }
    
    aux++;
  }while(*(aux - 1) != '\n');
  *aux = '\0';

}

void send_message_client(int socket, char* buffer){
  int ret;
  char* aux = buffer;
  
  ret = send(socket, (void*)aux, strlen(buffer), 0);
  if(ret == -1){
      exit_with_error("error: send()\n");
  }

}

void parse_and_elaborate_message(const char* message, faux_entry* e){
  char buf[BUFFER_SIZE];
  int ret;

  if(message[0] == '*'){
    message_size = atoi(message + 1);
    return;
  }
  
  else if(message[0] == '$'){
    segment_size = atoi(message + 1);
    return;
  }

  int i;
  for(i = 0; i < N_SUPPORTED_INSTRUCTIONS; i++){
  
    if(strncmp(message, instructions[i], strlen(instructions[i])) == 0){
      i_flags[i]++; 
      return;
    }
    
  }
  
  if(i_flags[0] == 1){
    memcpy(return_msg, "+OK\r\n", 6*sizeof(char));
    i_flags[0] = 0;
    return;
  }
  
  if(i_flags[1] == 1){
  
    ret = sem_wait(&database_access_semaphore);
    if(ret == -1){
      exit_with_error("error: sem_wait()\n");
    }
    
    Node* d_node = database_get_by_key(&database, message);
    
    ret = sem_post(&database_access_semaphore);
    if(ret == -1){
      exit_with_error("error: sem_post()\n");
    }
    
    if(d_node == NULL){
      memcpy(return_msg, "$-1\r\n", 6*sizeof(char));
      i_flags[1] = 0;
      return;
    }

    char* value = d_node -> entry.value;    
    i_flags[4] = 1;
    memcpy(return_msg, value, strlen(value) + 1);
    i_flags[1] = 0;
    return;
  }
  
  if(i_flags[2] == 1){  
    memcpy(e -> key, message, strlen(message) + 1);
    i_flags[2] = 2;
    return;
  }
  
  if(i_flags[2] == 2){      
    memcpy(e -> value, message, strlen(message) + 1);
    memset(return_msg, 0, BUFFER_SIZE);
    memcpy(return_msg, "+OK\r\n", 6*sizeof(char));
    i_flags[2] = 3;
    return;
  }
  
  if(i_flags[3] == 1){

    e -> lifetime = atoi(message);

    i_flags[3] = 0;
    return;
  }
  
  return;

}

void* connection_routine(void* t_args){
  thread_args* args = (thread_args*) t_args;
  int sock = args -> socket;
  char buf[BUFFER_SIZE];
  int ret;
  memset(buf, 0, BUFFER_SIZE);
  faux_entry e = {0};
  e.lifetime = ENTRY_LIFETIME_UNDET;
  while(1){
    
    while(message_size == 0){
      memset(buf, 0, BUFFER_SIZE);
      receive_message_client(sock, buf);
      
      if(strcmp(buf, END_MESSAGE) == 0){
        printf("thread stopping\n");
        close(sock);
        pthread_exit(NULL);
      }
      
      parse_and_elaborate_message(buf, &e);
    }
    
    int i;
    for(i = 0; i < message_size; i++){
      memset(buf, 0, BUFFER_SIZE);
      receive_message_client(sock, buf);
      parse_and_elaborate_message(buf, &e);
      
      if(segment_size == 0){
        exit_with_error("error: invalid syntax\n");
      }
      
      memset(buf, 0, BUFFER_SIZE);
      receive_message_client(sock, buf);
      segment_size = 0;
      parse_and_elaborate_message(buf, &e);
    }
    
    if(i_flags[2] == 3){
        
      ret = sem_wait(&database_access_semaphore);
      if(ret == -1){
        exit_with_error("error: sem_wait()\n");
      }
   
      Node* n = database_append(&database, e.key, e.value, e.lifetime);
      if(n == NULL){
        printf("error: database_append()\n");
      }
    
      ret = sem_post(&database_access_semaphore);
      if(ret == -1){
        exit_with_error("error: sem_post()\n");
      }
      
      i_flags[2] = 0;
    }
    
    if(i_flags[4] == 1){
      sprintf(buf, "$%ld\r\n", strlen(return_msg) - 2);
      printf("sending: %s\n", buf);
      send_message_client(sock, buf);
      i_flags[4] = 0;
    }
    
    printf("sending %s\n", return_msg);
    send_message_client(sock, return_msg);
    memset(return_msg, 0, BUFFER_SIZE);
    message_size = 0;
    memset(&e, 0, sizeof(faux_entry));
    e.lifetime = ENTRY_LIFETIME_UNDET;
  }

}

int main(int argc, const char * argv[]) {
    
    struct sigaction action = {0};
    action.sa_handler = int_handler;
    
    sigaction(SIGINT, &action, NULL);
    
    int ret;
    int yes = 1;
    database_init(&database);
    sem_init(&database_access_semaphore, 0, 1);
    
    int client_socket;

    struct sockaddr_in client_address;
    struct sockaddr_in server_address = {0};
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htons(INADDR_ANY);
    server_address.sin_port = htons(PORT);
    socklen_t addr_len = sizeof(struct sockaddr_in);
    
    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(0 > server_socket){
      exit_with_error("error: socket()\n");
    }
    
    ret = setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, (void*)&yes, sizeof(yes));
    if(0 > ret){
      exit_with_error("error: setsockopt()\n");
    }
    
    ret = bind(server_socket, (struct sockaddr*)&server_address, addr_len);
    if(0 > ret){
      exit_with_error("error: bind()\n");
    }
    
    ret = listen(server_socket, QUEUE_SIZE);
    if(0 > ret){
      exit_with_error("error: listen()\n");
    }    
    
    while(1){
    
      client_socket = accept(server_socket, (struct sockaddr*)&client_address, &addr_len);
      if(0 > client_socket){
        exit_with_error("error: accept()\n");
      }
      
      pthread_t thread;
      thread_args args = {.socket = client_socket, .addr = client_address};
      
      ret = pthread_create(&thread, NULL, connection_routine, (void*)&args);
      if(ret){
        errno = ret;
        exit_with_error("error: pthread_create\n");
      }
      
      ret = pthread_detach(thread);
      if(ret){
        errno = ret;
        exit_with_error("error: pthread_detach\n");
      }
      
    }
    
}