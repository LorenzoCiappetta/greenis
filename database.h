#define ENTRY_LIFETIME_UNDET -1

typedef struct Entry{
  char* key;
  char* value;
  int lifetime;
}Entry;

typedef struct Node{
  Entry entry;
  struct Node* next;
  struct Node* prev;
}Node;

typedef struct Database{
  Node* head;
  Node* tail;
  int size;
}Database;

void database_init(Database* database);
void entry_init(Entry* entry, const char* key, const char* value, int lifetime);
Node* database_append(Database* database, const char* key, const char* value, int lifetime);
Node* database_pop(Database* database);
Node* database_get_by_key(Database* database, const char* key);
Node* database_remove_by_key(Database* database, const char* key);
void database_destroy(Database* database);
void database_print(Database* database);