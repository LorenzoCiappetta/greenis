#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include "database.h"

typedef struct lifetime_handler_args{
  int lifetime;
  Database* database;
  char* key;
}lifetime_handler_args;

void* lifetime_handler(void* args){
  lifetime_handler_args* a = (lifetime_handler_args*)args;
  int lifetime = a -> lifetime;
  
  const char* key = a -> key;
  Database* database = a -> database;
 
  sleep(lifetime);
  database_print(database);
  database_remove_by_key(database, key);
  free(a -> key);
  free(a);
  pthread_exit(NULL);
}

void database_init(Database* database){
  if(database == NULL){
    return;
  }
  
  database -> head = NULL;
  database -> tail = NULL;
  database -> size = 0;
}

void entry_init(Entry* entry, const char* key, const char* value, int lifetime){
  if(entry == NULL){
    return;
  }

  int size_k = strlen(key) + 1;
  int size_v = strlen(value) + 1;
  
  entry -> key = malloc(size_k * sizeof(char));
  entry -> value = malloc(size_k * sizeof(char));
  entry -> lifetime = lifetime;
  
  memcpy(entry -> key, key, size_k);
  memcpy(entry -> value, value, size_v);
  
}

Node* database_append(Database* database, const char* key, const char* value, int lifetime){    
  if(database == NULL || key == NULL || value == NULL || lifetime < ENTRY_LIFETIME_UNDET){
    return NULL;
  }

  Node* new_n = malloc(sizeof(Node));
  new_n -> next = NULL;
  new_n -> prev = NULL;
  Entry* new_e = &new_n -> entry;
  entry_init(new_e, key, value, lifetime);  
  
  if(lifetime != ENTRY_LIFETIME_UNDET){
    int ret;
    lifetime_handler_args* args = malloc(sizeof(lifetime_handler_args));
    args -> lifetime = lifetime;
    args -> database = database;
    args -> key = malloc(strlen(key) + 1);
    memcpy(args -> key, key, strlen(key) + 1);
    pthread_t thread;
    
    ret = pthread_create(&thread, NULL, lifetime_handler, (void*)args);
    if(ret){
      printf("error: pthread_create()\n");
      errno = ret;
      _exit(-1);
    }
    
    ret = pthread_detach(thread);
    if(ret){
      printf("error: pthread_detach()\n");
      errno = ret;
      _exit(-1);
    }
  }
  
  if(database -> size == 0){
    database -> head = new_n;
    database -> tail = database -> head;
  }
  else{
    database -> tail -> next = new_n;
    new_n -> prev = database -> tail;
    database -> tail = new_n;
  }
  
  database -> size++;
  return new_n;
}

Node* database_pop(Database* database){
  if(database == NULL || database -> size == 0){
    return NULL;
  }
  Node* first = database -> head;
  Node* next = first -> next;
  database -> head = next;
   
  database -> size--;
  return first; 
}

Node* database_get_by_key(Database* database, const char* key){
  if(database == NULL || database -> size == 0){
    return NULL;
  }
  Node* current = database -> head;
  char* current_k = current -> entry.key;
  
  int size_c = strlen(current_k);
  int size_k = strlen(key);
  while(strncmp(current_k, key, size_k) != 0 || size_c != size_k){
    current = current -> next;
    if(current == NULL){
      return NULL;
    }
    current_k = current -> entry.key;
    size_c = strlen(current_k);
  }
  return current;
}

Node* database_remove_by_key(Database* database, const char* key){
  if(database == NULL || database -> size == 0){
    return NULL;
  }
  Node* rem = database_get_by_key(database, key);
  if(rem == NULL){
    return NULL;
  }
  else{
    Node* prev = rem -> prev;
    Node* next = rem -> next;
    if(prev != NULL){
      prev -> next = next;
    }
    if(next != NULL){
      next -> prev = prev;
    }
    if(rem == database -> head){
      database -> head = next;
    }
    if(rem == database -> tail){
      database -> tail = prev;
    }
  }
  
  database -> size--;
  return rem;
}

void database_destroy(Database* database){
  if(database == NULL){
    return;
  }
  Node* current;
  while(database -> size != 0){
    current = database_pop(database);
    free(current -> entry.key);
    free(current -> entry.value);
    free(current);
  }
}

void database_print(Database* database){
  if(database == NULL){
    return;
  }
  
  Node* current = database -> head;
  while(current != NULL){
    printf("%s\b\b: %s", current -> entry.key, current -> entry.value);
    current = current -> next;
  }
  
}
